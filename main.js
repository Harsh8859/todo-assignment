
const todoForm = document.getElementById('todo-form');
const todoInput = document.getElementById('todo-input');
const categorySelect = document.getElementById('category-select');
const personalList = document.getElementById('personal-tasks');
const booksList = document.getElementById('work-tasks');
const shoppingList = document.getElementById('shopping-tasks');

todoForm.addEventListener('submit', function(e) {
  e.preventDefault(); 


  const selectedCategory = categorySelect.value;
  const task = todoInput.value.trim();
  const taskItem = document.createElement('li');
  const deleteButton = document.createElement('button');
  deleteButton.textContent = 'Delete';
  deleteButton.classList.add('delete-btn');
  taskItem.textContent = task;
  taskItem.appendChild(deleteButton);
  switch (selectedCategory) {
    case 'personal':
      personalList.appendChild(taskItem);
      break;
    case 'books to read':
      booksList.appendChild(taskItem);
      break;
    case 'to buy':
      shoppingList.appendChild(taskItem);
      break;
    default:
      break;
  }
  todoInput.value = '';
});
personalList.addEventListener('click', function(e) {
  if (e.target.classList.contains('delete-btn')) {
    e.target.parentElement.remove();
  }
});

booksList.addEventListener('click', function(e) {
  if (e.target.classList.contains('delete-btn')) {
    e.target.parentElement.remove();
  }
});

shoppingList.addEventListener('click', function(e) {
  if (e.target.classList.contains('delete-btn')) {
    e.target.parentElement.remove();
  }
});

 